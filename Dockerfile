FROM alpine:3.18.3
RUN apk add --no-cache openjdk17
COPY build/libs/MS-24-User-Product-Service-0.0.1-SNAPSHOT.jar /app/MS-24-User-Product-Service-0.0.1-SNAPSHOT.jar
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/MS-24-User-Product-Service-0.0.1-SNAPSHOT.jar"]
