package org.ingress.ms24userproductservice.service;

import org.ingress.ms24userproductservice.dto.purchase.request.PurchaseBuyRequestDTO;
import org.ingress.ms24userproductservice.dto.purchase.response.PurchaseBuyResponseDTO;
import org.ingress.ms24userproductservice.dto.purchase.response.PurchaseRefundResponseDTO;
import org.ingress.ms24userproductservice.dto.purchase.response.PurchaseReportResponseDTO;

import java.util.List;

public interface PurchaseService {
    PurchaseBuyResponseDTO buyProduct(PurchaseBuyRequestDTO requestDTO);

    PurchaseRefundResponseDTO refundProduct(Long purchaseId);

    PurchaseReportResponseDTO reportByPurchaseId(Long purchaseId);

    List<PurchaseReportResponseDTO> reports(int page, int size);

    List<PurchaseReportResponseDTO> reportsByParameters();
}
