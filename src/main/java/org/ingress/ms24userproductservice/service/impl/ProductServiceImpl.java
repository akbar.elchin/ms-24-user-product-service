package org.ingress.ms24userproductservice.service.impl;

import lombok.RequiredArgsConstructor;
import org.ingress.ms24userproductservice.dto.product.ProductRequestDTO;
import org.ingress.ms24userproductservice.dto.product.ProductResponseDTO;
import org.ingress.ms24userproductservice.dto.product.ProductSearchCriteriaRequestDTO;
import org.ingress.ms24userproductservice.entity.Product;
import org.ingress.ms24userproductservice.exception.NotFoundException;
import org.ingress.ms24userproductservice.mapper.productMapper.ProductMapper;
import org.ingress.ms24userproductservice.repo.ProductRepository;
import org.ingress.ms24userproductservice.service.ProductService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.ingress.ms24userproductservice.exception.BusinessExceptionMessage.PRODUCT_NOT_FOUND_BY_ID;
import static org.ingress.ms24userproductservice.constants.Constant.SUCCESS;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductMapper productMapper;
    private final ProductRepository productRepository;

    @Override
    public ProductResponseDTO create(ProductRequestDTO requestDTO) {
        Product product = productRepository.save(productMapper.mapToEntity(requestDTO));
        return productMapper.mapToResponse(product);
    }

    @Override
    public ProductResponseDTO update(long id, ProductRequestDTO requestDTO) {
        Product product = productRepository.findById(id).orElseThrow(() -> new NotFoundException(PRODUCT_NOT_FOUND_BY_ID, id));
        product = productRepository.save(productMapper.mapToEntity(requestDTO, product));
        return productMapper.mapToResponse(product);
    }

    @Override
    public ProductResponseDTO getById(long id) {
        Product product = productRepository.findById(id).orElseThrow(() -> new NotFoundException(PRODUCT_NOT_FOUND_BY_ID, id));
        return productMapper.mapToResponse(product);
    }

    @Override
    public List<ProductResponseDTO> getAll(int page, int size) {
        List<Product> products = productRepository.findAll(PageRequest.of(page, size)).stream().toList();
        return productMapper.mapToResponseList(products);
    }

    @Override
    public String delete(long id) {
        productRepository.deleteById(id);
        return SUCCESS;
    }

    @Override
    public List<ProductResponseDTO> search(ProductSearchCriteriaRequestDTO requestDTO) {
        Specification<Product> productSpecification = productRepository.createSpecification(requestDTO);
        List<Product> products = productRepository.findAll(productSpecification,
                PageRequest.of(requestDTO.getPage(), requestDTO.getSize())).stream().toList();
        return productMapper.mapToResponseList(products);
    }
}
