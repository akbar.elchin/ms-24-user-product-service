package org.ingress.ms24userproductservice.service.impl;

import org.ingress.ms24userproductservice.entity.Product;
import org.ingress.ms24userproductservice.entity.Purchase;
import org.ingress.ms24userproductservice.entity.User;
import org.ingress.ms24userproductservice.exception.*;
import org.ingress.ms24userproductservice.service.ValidationService;
import org.ingress.ms24userproductservice.utiliy.CheckBalanceUtil;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.ingress.ms24userproductservice.exception.BusinessExceptionMessage.*;

@Service
public class ValidationServiceImpl implements ValidationService {
    @Override
    public User validateUser(Optional<User> optionalUser, long userId) {
        if (optionalUser.isEmpty()) {
            throw new NotFoundException(USER_NOT_FOUND_BY_ID, userId);
        }
        User user = optionalUser.get();
        if (!user.isStatus()) {
            throw new UserException(BusinessExceptionMessage.USER_IS_IN_ACTIVE, user.getId());
        }
        return optionalUser.get();
    }

    @Override
    public Product validateProduct(Optional<Product> productOptional, long productId) {
        if (productOptional.isEmpty()) {
            throw new NotFoundException(PRODUCT_NOT_FOUND_BY_ID, productId);
        }

        Product product = productOptional.get();
        if (product.getStockCount() < 1) {
            throw new ProductException(BusinessExceptionMessage.PRODUCT_IS_STOCK_OUT, product.getId());
        }
        return productOptional.get();
    }

    @Override
    public void validateBalance(BigDecimal totalPrice, User user) {
        if (CheckBalanceUtil.userBalanceIsNotEnough(totalPrice, user.getBalance())) {
            throw new UserException(BusinessExceptionMessage.USER_BALANCE_IS_NOT_ENOUGH, user.getId(), totalPrice);
        }
    }

    @Override
    public Purchase validationPurchase(Optional<Purchase> purchaseOptional, long purchaseId) {
        if (purchaseOptional.isEmpty()) {
            throw new NotFoundException(PURCHASE_NOT_FOUND_BY_ID, purchaseId);
        }
        Purchase purchase = purchaseOptional.get();
        if (purchase.getExpireRefundDate().isBefore(LocalDateTime.now())) {
            throw new PurchaseException(PURCHASE_EXPIRED_ALREADY, purchase.getExpireRefundDate(), LocalDateTime.now());
        }
        return purchase;
    }
}
