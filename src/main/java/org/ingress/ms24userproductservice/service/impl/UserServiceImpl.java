package org.ingress.ms24userproductservice.service.impl;

import lombok.RequiredArgsConstructor;
import org.ingress.ms24userproductservice.dto.userDto.UserRequestDTO;
import org.ingress.ms24userproductservice.dto.userDto.UserResponseDTO;
import org.ingress.ms24userproductservice.dto.userDto.UserSearchCriteriaRequestDTO;
import org.ingress.ms24userproductservice.entity.User;
import org.ingress.ms24userproductservice.entity.UserBalance;
import org.ingress.ms24userproductservice.exception.NotFoundException;
import org.ingress.ms24userproductservice.mapper.userMapper.UserMapper;
import org.ingress.ms24userproductservice.repo.UserRepository;
import org.ingress.ms24userproductservice.service.UserService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.ingress.ms24userproductservice.exception.BusinessExceptionMessage.USER_NOT_FOUND_BY_ID;
import static org.ingress.ms24userproductservice.exception.BusinessExceptionMessage.USER_NOT_FOUND_BY_USERNAME;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;
    private final UserRepository userRepository;

    @Override
    public UserResponseDTO create(UserRequestDTO requestDTO) {
        User user = userMapper.mapToEntity(requestDTO);
        user = userRepository.save(user);
        return userMapper.mapToResponse(user);
    }

    @Override
    public UserResponseDTO update(Long id, UserRequestDTO requestDTO) {
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException(USER_NOT_FOUND_BY_ID, id));
        userMapper.mapToEntity(requestDTO, user);
        user = userRepository.save(user);
        return userMapper.mapToResponse(user);
    }

    @Override
    public UserResponseDTO updateStatus(Long id, boolean status) {
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException(USER_NOT_FOUND_BY_ID, id));
        user.setStatus(status);
        user = userRepository.save(user);
        return userMapper.mapToResponse(user);
    }

    @Override
    public UserResponseDTO getUserById(Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException(USER_NOT_FOUND_BY_ID, id));
        return userMapper.mapToResponse(user);
    }

    @Override
    public UserResponseDTO getUserByUsername(String username) {
        User user = userRepository.getUserByUsername(username)
                .orElseThrow(() -> new NotFoundException(USER_NOT_FOUND_BY_USERNAME, username));
        return userMapper.mapToResponse(user);
    }

    @Override
    public BigDecimal getBalanceByUsername(String username) {
        Optional<UserBalance> userBalance = userRepository.findBalanceByUsername(username);
        if (userBalance.isPresent()) {
            return userBalance.get().getBalance();
        }
        throw new NotFoundException(USER_NOT_FOUND_BY_USERNAME, username);
    }

    @Override
    public List<UserResponseDTO> getAll(int page, int size) {
        List<User> users = userRepository.findAll(PageRequest.of(page, size)).stream().collect(Collectors.toList());
        return userMapper.mapToResponseList(users);
    }

    @Override
    public List<UserResponseDTO> getAllByStatus(int page, int size, boolean status) {
        List<User> users = userRepository.getAllByStatus(PageRequest.of(page, size), status);
        return userMapper.mapToResponseList(users);
    }

    @Override
    public List<UserResponseDTO> searchUser(UserSearchCriteriaRequestDTO requestDTO) {
        Specification<User> userSpecification = userRepository.createSpecification(requestDTO);
        List<User> users = userRepository.findAll(userSpecification,
                PageRequest.of(requestDTO.getPage(), requestDTO.getSize())).stream().toList();
        return userMapper.mapToResponseList(users);
    }
}
