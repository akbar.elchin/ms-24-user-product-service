package org.ingress.ms24userproductservice.service.impl;

import lombok.RequiredArgsConstructor;
import org.ingress.ms24userproductservice.dto.purchase.request.PurchaseBuyRequestDTO;
import org.ingress.ms24userproductservice.dto.purchase.response.PurchaseBuyResponseDTO;
import org.ingress.ms24userproductservice.dto.purchase.response.PurchaseRefundResponseDTO;
import org.ingress.ms24userproductservice.dto.purchase.response.PurchaseReportResponseDTO;
import org.ingress.ms24userproductservice.entity.Product;
import org.ingress.ms24userproductservice.entity.Purchase;
import org.ingress.ms24userproductservice.entity.User;
import org.ingress.ms24userproductservice.mapper.purchaseMapper.PurchaseMapper;
import org.ingress.ms24userproductservice.repo.ProductRepository;
import org.ingress.ms24userproductservice.repo.PurchaseRepository;
import org.ingress.ms24userproductservice.repo.UserRepository;
import org.ingress.ms24userproductservice.service.PurchaseService;
import org.ingress.ms24userproductservice.service.ValidationService;
import org.ingress.ms24userproductservice.utiliy.CalculateUtil;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PurchaseServiceImpl implements PurchaseService {

    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final PurchaseRepository purchaseRepository;
    private final PurchaseMapper purchaseMapper;
    private final ValidationService validationService;

    @Override
    @Transactional
    public PurchaseBuyResponseDTO buyProduct(PurchaseBuyRequestDTO requestDTO) {
        Optional<User> userOptional = userRepository.findById(requestDTO.getUserId());
        User user = validationService.validateUser(userOptional, requestDTO.getUserId());

        Optional<Product> productOptional = productRepository.findById(requestDTO.getProductId());
        Product product = validationService.validateProduct(productOptional, requestDTO.getProductId());

        BigDecimal totalPrice = calculatePrice(requestDTO, product);
        validationService.validateBalance(totalPrice, user);

        Purchase purchase = purchaseMapper.mapToEntity(requestDTO, user, product, totalPrice);
        purchase = purchaseRepository.save(purchase);

        decreaseProductStockCount(product);
        decreaseUserBalance(user, totalPrice);

        return purchaseMapper.mapToResponse(purchase);
    }

    @Override
    @Transactional
    public PurchaseRefundResponseDTO refundProduct(Long purchaseId) {
        Optional<Purchase> purchaseOptional = purchaseRepository.findById(purchaseId);
        Purchase purchase = validationService.validationPurchase(purchaseOptional, purchaseId);

        increaseProductStockCount(purchase.getProduct().getId());
        increaseUserBalance(purchase.getUser().getId(), purchase.getTotalPrice());

        purchase.setRefund(true);
        purchase.setRefundTime(LocalDateTime.now());
        purchase = purchaseRepository.save(purchase);

        return purchaseMapper.mapToRefundResponse(purchase);
    }

    @Override
    public PurchaseReportResponseDTO reportByPurchaseId(Long purchaseId) {
        Optional<Purchase> purchaseOptional = purchaseRepository.findById(purchaseId);
        Purchase purchase = validationService.validationPurchase(purchaseOptional, purchaseId);
        return purchaseMapper.mapToReportResponse(purchase);
    }

    @Override
    public List<PurchaseReportResponseDTO> reports(int page, int size) {
        List<Purchase> purchases = purchaseRepository.findAll(PageRequest.of(page, size)).stream().toList();
        return purchaseMapper.mapToReportResponseList(purchases);
    }

    @Override
    public List<PurchaseReportResponseDTO> reportsByParameters() {
        return null;
    }

    private BigDecimal calculatePrice(PurchaseBuyRequestDTO requestDTO, Product product) {
        return CalculateUtil.calculatePrice(requestDTO.getQuantity(), requestDTO.getDiscount(), product.getPrice());
    }

    private void decreaseProductStockCount(Product product) {
        product.setStockCount(product.getStockCount() - 1);
        productRepository.save(product);
    }

    private void increaseProductStockCount(long productId) {
        productRepository.increaseStockCountById(productId, 1);
    }

    private void decreaseUserBalance(User user, BigDecimal amount) {
        user.setBalance(user.getBalance().subtract(amount));
        userRepository.save(user);
    }

    private void increaseUserBalance(long userId, BigDecimal amount) {
        userRepository.increaseBalanceById(userId, amount);
    }
}
