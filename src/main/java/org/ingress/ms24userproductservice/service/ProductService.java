package org.ingress.ms24userproductservice.service;

import org.ingress.ms24userproductservice.dto.product.ProductRequestDTO;
import org.ingress.ms24userproductservice.dto.product.ProductResponseDTO;
import org.ingress.ms24userproductservice.dto.product.ProductSearchCriteriaRequestDTO;

import java.util.List;

public interface ProductService {

    ProductResponseDTO create(ProductRequestDTO requestDTO);

    ProductResponseDTO update(long id, ProductRequestDTO requestDTO);

    ProductResponseDTO getById(long id);

    List<ProductResponseDTO> getAll(int page, int size);

    String delete(long id);
    List<ProductResponseDTO> search(ProductSearchCriteriaRequestDTO requestDTO);
}
