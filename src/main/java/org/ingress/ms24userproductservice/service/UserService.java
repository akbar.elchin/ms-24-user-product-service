package org.ingress.ms24userproductservice.service;

import org.ingress.ms24userproductservice.dto.userDto.UserRequestDTO;
import org.ingress.ms24userproductservice.dto.userDto.UserResponseDTO;
import org.ingress.ms24userproductservice.dto.userDto.UserSearchCriteriaRequestDTO;

import java.math.BigDecimal;
import java.util.List;

public interface UserService {

    UserResponseDTO create(UserRequestDTO requestDTO);

    UserResponseDTO update(Long id, UserRequestDTO requestDTO);

    UserResponseDTO updateStatus(Long id, boolean status);

    UserResponseDTO getUserById(Long id);

    UserResponseDTO getUserByUsername(String username);

    BigDecimal getBalanceByUsername(String username);

    List<UserResponseDTO> getAll(int page, int size);

    List<UserResponseDTO> getAllByStatus(int page, int size, boolean status);

    List<UserResponseDTO> searchUser(UserSearchCriteriaRequestDTO requestDTO);
}
