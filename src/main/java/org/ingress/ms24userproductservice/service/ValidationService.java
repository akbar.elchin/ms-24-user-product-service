package org.ingress.ms24userproductservice.service;

import org.ingress.ms24userproductservice.entity.Product;
import org.ingress.ms24userproductservice.entity.Purchase;
import org.ingress.ms24userproductservice.entity.User;

import java.math.BigDecimal;
import java.util.Optional;

public interface ValidationService {

    User validateUser(Optional<User> optionalUser,long userId);

    Product validateProduct(Optional<Product> productOptional, long productId);

    void validateBalance(BigDecimal totalPrice, User user);

    Purchase validationPurchase(Optional<Purchase> purchaseOptional, long purchaseId);
}
