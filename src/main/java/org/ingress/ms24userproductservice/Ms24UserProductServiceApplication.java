package org.ingress.ms24userproductservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms24UserProductServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ms24UserProductServiceApplication.class, args);
    }

}
