package org.ingress.ms24userproductservice.utiliy;

import lombok.experimental.UtilityClass;

import java.math.BigDecimal;
import java.math.RoundingMode;

@UtilityClass
public class CalculateUtil {

    public static BigDecimal calculatePrice(int quantity, int discount, BigDecimal price) {
        BigDecimal totalPrice = price.multiply(BigDecimal.valueOf(quantity));
        BigDecimal dis = totalPrice.multiply(BigDecimal.valueOf(discount)).divide(BigDecimal.valueOf(100), RoundingMode.HALF_UP);
        return totalPrice.subtract(dis);
    }
}
