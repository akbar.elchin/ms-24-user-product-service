package org.ingress.ms24userproductservice.utiliy;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ExceptionApi {

    long errorTime;
    String message;
    String status;
    int code;
}
