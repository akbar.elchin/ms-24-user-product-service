package org.ingress.ms24userproductservice.utiliy.searchBuilder;

import org.ingress.ms24userproductservice.dto.userDto.UserSearchCriteriaRequestDTO;
import org.ingress.ms24userproductservice.exception.BusinessExceptionMessage;
import org.ingress.ms24userproductservice.exception.ComparatorException;
import org.ingress.ms24userproductservice.repo.repoSpecification.ComparisonOperator;
import org.ingress.ms24userproductservice.utiliy.DateConverter;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;

import static org.ingress.ms24userproductservice.exception.BusinessExceptionMessage.COMPARE_OPERATOR_INVALID;

public interface UserSearchBuilder {

    static UserSearchCriteriaRequestDTO buildSearchRequest(String username,
                                                           String name,
                                                           String surname,
                                                           String birthDateStart,
                                                           String birthDateEnd,
                                                           ComparisonOperator birthDateOperator,
                                                           BigDecimal balanceStart,
                                                           BigDecimal balanceEnd,
                                                           ComparisonOperator balanceOperator,
                                                           Boolean status,
                                                           String creatAtStart,
                                                           String creatAtEnd,
                                                           ComparisonOperator creationTimeOperator,
                                                           int page,
                                                           int size) {
        UserSearchCriteriaRequestDTO requestDTO = new UserSearchCriteriaRequestDTO();
        requestDTO.setUsername(username);
        requestDTO.setName(name);
        requestDTO.setSurname(surname);
        if (!ObjectUtils.isEmpty(birthDateStart)) {
            requestDTO.setBirthDateStart(DateConverter.convertToDate(birthDateStart));
        }
        if (!ObjectUtils.isEmpty(birthDateEnd)) {
            requestDTO.setBirthDateEnd(DateConverter.convertToDate(birthDateEnd));
        }
        requestDTO.setBirthDateComparisonOperator(birthDateComparison(birthDateStart, birthDateEnd, birthDateOperator));
        requestDTO.setBalanceStart(balanceStart);
        requestDTO.setBalanceEnd(balanceEnd);
        requestDTO.setBalanceComparisonOperator(balanceComparison(balanceStart, balanceEnd, balanceOperator));
        requestDTO.setStatus(status);
        if (!ObjectUtils.isEmpty(creatAtStart)) {
            requestDTO.setCreatAtStart(DateConverter.convertToDateTime(creatAtStart));
        }
        if (!ObjectUtils.isEmpty(creatAtEnd)) {
            requestDTO.setCreatAtEnd(DateConverter.convertToDateTime(creatAtEnd));
        }
        requestDTO.setCreationTimeComparisonOperator(creatTimeComparison(creatAtStart, creatAtEnd, creationTimeOperator));
        requestDTO.setPage(page);
        requestDTO.setSize(size);
        return requestDTO;
    }

    private static ComparisonOperator birthDateComparison(String birthDateStart,
                                                          String birthDateEnd,
                                                          ComparisonOperator birthDateOperator) {
        if (!ObjectUtils.isEmpty(birthDateStart) && !ObjectUtils.isEmpty(birthDateEnd)) {
            return ComparisonOperator.BETWEEN;
        } else if (ObjectUtils.isEmpty(birthDateEnd)) {
            return birthDateOperator == null ? ComparisonOperator.GREATER_THAN : birthDateOperator;
        }
        throw new ComparatorException(BusinessExceptionMessage.valueOf(COMPARE_OPERATOR_INVALID.getMessage()), "birth date");
    }

    private static ComparisonOperator balanceComparison(BigDecimal balanceStart,
                                                        BigDecimal balanceEnd,
                                                        ComparisonOperator balanceOperator) {
        if (!ObjectUtils.isEmpty(balanceStart) && !ObjectUtils.isEmpty(balanceEnd)) {
            return ComparisonOperator.BETWEEN;
        } else if (ObjectUtils.isEmpty(balanceEnd)) {
            return balanceOperator == null ? ComparisonOperator.GREATER_THAN : balanceOperator;
        }
        throw new ComparatorException(BusinessExceptionMessage.valueOf(COMPARE_OPERATOR_INVALID.getMessage()), "balance");
    }

    private static ComparisonOperator creatTimeComparison(String creatAtStart,
                                                          String creatAtEnd,
                                                          ComparisonOperator creationTimeOperator) {
        if (!ObjectUtils.isEmpty(creatAtStart) && !ObjectUtils.isEmpty(creatAtEnd)) {
            return ComparisonOperator.BETWEEN;
        } else if (ObjectUtils.isEmpty(creatAtEnd)) {
            return creationTimeOperator == null ? ComparisonOperator.GREATER_THAN : creationTimeOperator;
        }
        throw new ComparatorException(BusinessExceptionMessage.valueOf(COMPARE_OPERATOR_INVALID.getMessage()), "creation time");
    }
}
