package org.ingress.ms24userproductservice.utiliy.searchBuilder;

import org.ingress.ms24userproductservice.dto.product.ProductSearchCriteriaRequestDTO;
import org.ingress.ms24userproductservice.exception.BusinessExceptionMessage;
import org.ingress.ms24userproductservice.exception.ComparatorException;
import org.ingress.ms24userproductservice.repo.repoSpecification.ComparisonOperator;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;

import static org.ingress.ms24userproductservice.exception.BusinessExceptionMessage.COMPARE_OPERATOR_INVALID;

public interface ProductSearchBuilder {

    static ProductSearchCriteriaRequestDTO createBuilder(String name,
                                                         BigDecimal priceStart,
                                                         BigDecimal priceEnd,
                                                         ComparisonOperator priceOperator,
                                                         Integer stockCountStart,
                                                         Integer stockCountEnd,
                                                         ComparisonOperator stockCountOperator,
                                                         int page,
                                                         int size) {
        ProductSearchCriteriaRequestDTO requestDTO = new ProductSearchCriteriaRequestDTO();
        requestDTO.setName(name);
        requestDTO.setPriceStart(priceStart);
        requestDTO.setPriceEnd(priceEnd);
        requestDTO.setPriceOperator(priceComparison(priceStart, priceEnd, priceOperator));
        requestDTO.setStockCountStart(stockCountStart);
        requestDTO.setStockCountEnd(stockCountEnd);
        requestDTO.setStockCountOperator(stockCountComparison(stockCountStart, stockCountEnd, stockCountOperator));
        requestDTO.setPage(page);
        requestDTO.setSize(size);
        return requestDTO;
    }

    private static ComparisonOperator priceComparison(BigDecimal priceStart,
                                                      BigDecimal priceEnd,
                                                      ComparisonOperator priceOperator) {
        if (!ObjectUtils.isEmpty(priceStart) && !ObjectUtils.isEmpty(priceEnd)) {
            return ComparisonOperator.BETWEEN;
        } else if (ObjectUtils.isEmpty(priceEnd)) {
            return priceOperator == null ? ComparisonOperator.GREATER_THAN : priceOperator;
        }
        throw new ComparatorException(BusinessExceptionMessage.valueOf(COMPARE_OPERATOR_INVALID.getMessage()), "price");
    }

    private static ComparisonOperator stockCountComparison(Integer stockCountStart,
                                                           Integer stockCountEnd,
                                                           ComparisonOperator stockCountOperator) {
        if (!ObjectUtils.isEmpty(stockCountStart) && !ObjectUtils.isEmpty(stockCountEnd)) {
            return ComparisonOperator.BETWEEN;
        } else if (ObjectUtils.isEmpty(stockCountEnd)) {
            return stockCountOperator == null ? ComparisonOperator.GREATER_THAN : stockCountOperator;
        }
        throw new ComparatorException(BusinessExceptionMessage.valueOf(COMPARE_OPERATOR_INVALID.getMessage()), "stock count");
    }
}
