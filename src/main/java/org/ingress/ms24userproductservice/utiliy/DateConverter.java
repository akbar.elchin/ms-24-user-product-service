package org.ingress.ms24userproductservice.utiliy;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.ingress.ms24userproductservice.exception.BusinessExceptionMessage.DATE_TIME_FORMAT_INVALID;

@Slf4j
@UtilityClass
public class DateConverter {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public static LocalDateTime convertToDateTime(String date) {
        try {
            return LocalDateTime.parse(date, DATE_TIME_FORMATTER);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new DateTimeException(DATE_TIME_FORMAT_INVALID.getMessage());
        }
    }

    public static LocalDate convertToDate(String date) {
        try {
            return LocalDate.parse(date, DATE_FORMATTER);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new DateTimeException(DATE_TIME_FORMAT_INVALID.getMessage());
        }
    }
}
