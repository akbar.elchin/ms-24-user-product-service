package org.ingress.ms24userproductservice.utiliy;

import lombok.experimental.UtilityClass;

import java.math.BigDecimal;

@UtilityClass
public class CheckBalanceUtil {
    public static boolean userBalanceIsNotEnough(BigDecimal totalPrice, BigDecimal userBalance) {
        return totalPrice.compareTo(userBalance) > 0;
    }
}
