package org.ingress.ms24userproductservice.resource;

import lombok.RequiredArgsConstructor;
import org.ingress.ms24userproductservice.dto.product.ProductRequestDTO;
import org.ingress.ms24userproductservice.dto.product.ProductResponseDTO;
import org.ingress.ms24userproductservice.repo.repoSpecification.ComparisonOperator;
import org.ingress.ms24userproductservice.service.ProductService;
import org.ingress.ms24userproductservice.utiliy.searchBuilder.ProductSearchBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductResource {

    private final ProductService productService;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ProductResponseDTO> create(@RequestBody ProductRequestDTO requestDTO) {
        ProductResponseDTO responseDTO = productService.create(requestDTO);
        return ResponseEntity.ok(responseDTO);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ProductResponseDTO> update(@PathVariable long id,
                                                     @RequestBody ProductRequestDTO requestDTO) {
        ProductResponseDTO responseDTO = productService.update(id, requestDTO);
        return ResponseEntity.ok(responseDTO);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ProductResponseDTO> getById(@PathVariable long id) {
        ProductResponseDTO responseDTO = productService.getById(id);
        return ResponseEntity.ok(responseDTO);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ProductResponseDTO>> getAll(@RequestParam(defaultValue = "0") int page,
                                                           @RequestParam(defaultValue = "20") int size) {
        List<ProductResponseDTO> responseDTOs = productService.getAll(page, size);
        return ResponseEntity.ok(responseDTOs);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> delete(@PathVariable long id) {
        String message = productService.delete(id);
        return ResponseEntity.ok(message);
    }

    @GetMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ProductResponseDTO>> search(@RequestParam(required = false) String name,
                                                           @RequestParam(required = false) BigDecimal priceStart,
                                                           @RequestParam(required = false) BigDecimal priceEnd,
                                                           @RequestParam(required = false) ComparisonOperator priceOperator,
                                                           @RequestParam(required = false) Integer stockCountStart,
                                                           @RequestParam(required = false) Integer stockCountEnd,
                                                           @RequestParam(required = false) ComparisonOperator stockCountOperator,
                                                           @RequestParam(defaultValue = "0") int page,
                                                           @RequestParam(defaultValue = "20") int size) {
        List<ProductResponseDTO> productResponseDTOS = productService.search(ProductSearchBuilder.createBuilder(
                name, priceStart, priceEnd, priceOperator, stockCountStart, stockCountEnd, stockCountOperator, page, size));
        return ResponseEntity.ok(productResponseDTOS);
    }
}
