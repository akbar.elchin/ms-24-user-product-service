package org.ingress.ms24userproductservice.resource;

import lombok.RequiredArgsConstructor;
import org.ingress.ms24userproductservice.dto.userDto.UserRequestDTO;
import org.ingress.ms24userproductservice.dto.userDto.UserResponseDTO;
import org.ingress.ms24userproductservice.repo.repoSpecification.ComparisonOperator;
import org.ingress.ms24userproductservice.service.UserService;
import org.ingress.ms24userproductservice.utiliy.searchBuilder.UserSearchBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserResource {

    private final UserService userService;

    @PostMapping
    public ResponseEntity<UserResponseDTO> create(@RequestBody UserRequestDTO userRequestDTO) {
        UserResponseDTO responseDTO = userService.create(userRequestDTO);
        return ResponseEntity.ok(responseDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserResponseDTO> update(@PathVariable long id, @RequestBody UserRequestDTO userRequestDTO) {
        UserResponseDTO responseDTO = userService.update(id, userRequestDTO);
        return ResponseEntity.ok(responseDTO);
    }

    @PatchMapping("/{id}/{status}")
    public ResponseEntity<UserResponseDTO> updateStatus(@PathVariable long id, @PathVariable boolean status) {
        UserResponseDTO responseDTO = userService.updateStatus(id, status);
        return ResponseEntity.ok(responseDTO);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserResponseDTO> getById(@PathVariable long id) {
        UserResponseDTO responseDTO = userService.getUserById(id);
        return ResponseEntity.ok(responseDTO);
    }

    @GetMapping("by-username/{username}")
    public ResponseEntity<UserResponseDTO> getByUsername(@PathVariable String username) {
        UserResponseDTO responseDTO = userService.getUserByUsername(username);
        return ResponseEntity.ok(responseDTO);
    }

    @GetMapping("balance/{username}")
    public ResponseEntity<BigDecimal> getBalanceByUsername(@PathVariable String username) {
        BigDecimal balance = userService.getBalanceByUsername(username);
        return ResponseEntity.ok(balance);
    }


    @GetMapping
    public ResponseEntity<List<UserResponseDTO>> getByUsers(@RequestParam(defaultValue = "0") int page,
                                                            @RequestParam(defaultValue = "20") int size) {
        List<UserResponseDTO> responseDTOList = userService.getAll(page, size);
        return ResponseEntity.ok(responseDTOList);
    }

    @GetMapping("/by-status")
    public ResponseEntity<List<UserResponseDTO>> getByUsersByStatus(@RequestParam(defaultValue = "0") int page,
                                                                    @RequestParam(defaultValue = "20") int size,
                                                                    @RequestParam(defaultValue = "true") boolean status) {
        List<UserResponseDTO> responseDTOList = userService.getAllByStatus(page, size, status);
        return ResponseEntity.ok(responseDTOList);
    }

    @GetMapping("/search")
    public ResponseEntity<List<UserResponseDTO>> search(@RequestParam(required = false) String username,
                                                        @RequestParam(required = false) String name,
                                                        @RequestParam(required = false) String surname,
                                                        @RequestParam(required = false) String birthDateStart,
                                                        @RequestParam(required = false) String birthDateEnd,
                                                        @RequestParam(required = false) ComparisonOperator birthDateOperator,
                                                        @RequestParam(required = false) BigDecimal balanceStart,
                                                        @RequestParam(required = false) BigDecimal balanceEnd,
                                                        @RequestParam(required = false) ComparisonOperator balanceOperator,
                                                        @RequestParam(required = false,defaultValue = "true") Boolean status,
                                                        @RequestParam(required = false) String creatAtStart,
                                                        @RequestParam(required = false) String creatAtEnd,
                                                        @RequestParam(required = false) ComparisonOperator creationTimeOperator,
                                                        @RequestParam(defaultValue = "0") int page,
                                                        @RequestParam(defaultValue = "20") int size) {
        List<UserResponseDTO> responseDTOList = userService.searchUser(UserSearchBuilder.buildSearchRequest(username, name,
                surname, birthDateStart, birthDateEnd, birthDateOperator, balanceStart, balanceEnd,
                balanceOperator, status, creatAtStart, creatAtEnd, creationTimeOperator, page, size));
        return ResponseEntity.ok(responseDTOList);
    }
}
