package org.ingress.ms24userproductservice.resource;

import lombok.RequiredArgsConstructor;
import org.ingress.ms24userproductservice.dto.purchase.request.PurchaseBuyRequestDTO;
import org.ingress.ms24userproductservice.dto.purchase.response.PurchaseBuyResponseDTO;
import org.ingress.ms24userproductservice.dto.purchase.response.PurchaseRefundResponseDTO;
import org.ingress.ms24userproductservice.dto.purchase.response.PurchaseReportResponseDTO;
import org.ingress.ms24userproductservice.service.PurchaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/purchase")
@RequiredArgsConstructor
public class PurchaseResource {
    private final PurchaseService purchaseService;

    @PostMapping("/buy")
    public ResponseEntity<PurchaseBuyResponseDTO> buyProduct(@RequestBody PurchaseBuyRequestDTO requestDTO) {
        PurchaseBuyResponseDTO responseDTO = purchaseService.buyProduct(requestDTO);
        return ResponseEntity.ok(responseDTO);
    }

    @PutMapping("/refund/{purchaseId}")
    public ResponseEntity<PurchaseRefundResponseDTO> refundProduct(@PathVariable long purchaseId) {
        PurchaseRefundResponseDTO responseDTO = purchaseService.refundProduct(purchaseId);
        return ResponseEntity.ok(responseDTO);
    }

    @GetMapping("/{purchaseId}")
    public ResponseEntity<PurchaseReportResponseDTO> getByPurchaseId(@PathVariable long purchaseId) {
        PurchaseReportResponseDTO responseDTO = purchaseService.reportByPurchaseId(purchaseId);
        return ResponseEntity.ok(responseDTO);
    }

    @GetMapping
    public ResponseEntity<List<PurchaseReportResponseDTO>> getAll(@RequestParam(defaultValue = "0") int page,
                                                                  @RequestParam(defaultValue = "20") int size) {
        List<PurchaseReportResponseDTO> reportResponseDTOS = purchaseService.reports(page, size);
        return ResponseEntity.ok(reportResponseDTOS);
    }
}
