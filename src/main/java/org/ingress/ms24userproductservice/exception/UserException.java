package org.ingress.ms24userproductservice.exception;

public class UserException extends RuntimeException{

    public UserException(BusinessExceptionMessage message, Object... params) {
        super(message.getMessage(params));
    }
}
