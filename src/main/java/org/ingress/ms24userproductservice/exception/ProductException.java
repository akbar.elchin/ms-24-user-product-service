package org.ingress.ms24userproductservice.exception;

public class ProductException extends RuntimeException{

    public ProductException(BusinessExceptionMessage message, Object... params) {
        super(message.getMessage(params));
    }
}
