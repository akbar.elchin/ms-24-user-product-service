package org.ingress.ms24userproductservice.exception;

public class PurchaseException extends RuntimeException{

    public PurchaseException(BusinessExceptionMessage message, Object... params) {
        super(message.getMessage(params));
    }
}
