package org.ingress.ms24userproductservice.exception;

public class ComparatorException extends RuntimeException{

    public ComparatorException(BusinessExceptionMessage message, Object... params) {
        super(message.getMessage(params));
    }
}
