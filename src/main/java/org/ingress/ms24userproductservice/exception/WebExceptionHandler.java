package org.ingress.ms24userproductservice.exception;

import lombok.extern.slf4j.Slf4j;
import org.ingress.ms24userproductservice.utiliy.ExceptionApi;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.DateTimeException;


@Slf4j
@RestControllerAdvice
public class WebExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ExceptionApi> handleCustomException(NotFoundException ex) {
        log.error("Exception : {}", ex.getMessage());
        return ResponseEntity.ok(ExceptionApi.builder()
                .errorTime(System.currentTimeMillis())
                .status(HttpStatus.BAD_REQUEST.name())
                .code(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .build());
    }

    @ExceptionHandler(UserException.class)
    public ResponseEntity<ExceptionApi> handleCustomException(UserException ex) {
        log.error("Exception : {}", ex.getMessage());
        return ResponseEntity.ok(ExceptionApi.builder()
                .errorTime(System.currentTimeMillis())
                .status(HttpStatus.BAD_REQUEST.name())
                .code(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .build());
    }

    @ExceptionHandler(ProductException.class)
    public ResponseEntity<ExceptionApi> handleCustomException(ProductException ex) {
        log.error("Exception : {}", ex.getMessage());
        return ResponseEntity.ok(ExceptionApi.builder()
                .errorTime(System.currentTimeMillis())
                .status(HttpStatus.BAD_REQUEST.name())
                .code(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .build());
    }

    @ExceptionHandler(PurchaseException.class)
    public ResponseEntity<ExceptionApi> handleCustomException(PurchaseException ex) {
        log.error("Exception : {}", ex.getMessage());
        return ResponseEntity.ok(ExceptionApi.builder()
                .errorTime(System.currentTimeMillis())
                .status(HttpStatus.BAD_REQUEST.name())
                .code(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .build());
    }

    @ExceptionHandler(ComparatorException.class)
    public ResponseEntity<ExceptionApi> handleCustomException(ComparatorException ex) {
        log.error("Exception : {}", ex.getMessage());
        return ResponseEntity.ok(ExceptionApi.builder()
                .errorTime(System.currentTimeMillis())
                .status(HttpStatus.BAD_REQUEST.name())
                .code(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .build());
    }


    @ExceptionHandler(DateTimeException.class)
    public ResponseEntity<ExceptionApi> handleCustomException(DateTimeException ex) {
        log.error("Exception : {}", ex.getMessage());
        return ResponseEntity.ok(ExceptionApi.builder()
                .errorTime(System.currentTimeMillis())
                .status(HttpStatus.BAD_REQUEST.name())
                .code(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .build());
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ExceptionApi> handleCustomException(RuntimeException ex) {
        log.error("Exception : {}", ex.getMessage());
        return ResponseEntity.ok(ExceptionApi.builder()
                .errorTime(System.currentTimeMillis())
                .status(HttpStatus.INTERNAL_SERVER_ERROR.name())
                .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message(ex.getMessage())
                .build());
    }
}
