package org.ingress.ms24userproductservice.exception;

public class NotFoundException extends RuntimeException {

    public NotFoundException(BusinessExceptionMessage message, Object... params) {
        super(message.getMessage(params));
    }
}
