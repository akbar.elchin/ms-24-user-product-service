package org.ingress.ms24userproductservice.exception;

public enum BusinessExceptionMessage {
    USER_NOT_FOUND_BY_ID(1, "User Not Found By Id: %s"),
    USER_NOT_FOUND_BY_USERNAME(2, "User Not Found By Username: %s"),
    USER_IS_IN_ACTIVE(3, "User By Id: %s is in active status"),
    USER_BALANCE_IS_NOT_ENOUGH(4, "User By Id: %s is not enough balance. Required: %s amount"),
    PRODUCT_NOT_FOUND_BY_ID(5, "Product Not Found By Id: %s"),
    PRODUCT_IS_STOCK_OUT(6, "Product by Id: %s is stock out"),
    PURCHASE_NOT_FOUND_BY_ID(7, "Purchase Not Found By Id: %s"),
    PURCHASE_EXPIRED_ALREADY(8, "Purchase is expired.Last Date is %s and now is %s"),
    DATE_TIME_FORMAT_INVALID(9, "Date Time Format invalid"),
    COMPARE_OPERATOR_INVALID(10, "Compare Operator: %s is empty or invalid");

    BusinessExceptionMessage(int id, String message) {
        this.id = id;
        this.message = message;
    }

    private int id;
    private String message;

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public String getMessage(Object... params) {
        return String.format(message, params);
    }
}
