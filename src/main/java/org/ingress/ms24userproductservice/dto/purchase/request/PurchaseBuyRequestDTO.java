package org.ingress.ms24userproductservice.dto.purchase.request;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PurchaseBuyRequestDTO {
    long userId;
    long productId;
    int quantity;
    int discount;
}
