package org.ingress.ms24userproductservice.dto.purchase.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PurchaseBuyResponseDTO {
    long purchaseId;
    long userId;
    long productId;
    String productName;
    BigDecimal productPrice;
    int discount;
    int quantity;
    BigDecimal totalPrice;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    LocalDateTime purchaseTime;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    LocalDateTime expiredTime;
}
