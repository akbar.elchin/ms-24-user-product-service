package org.ingress.ms24userproductservice.dto.purchase.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.ingress.ms24userproductservice.dto.product.ProductResponseDTO;
import org.ingress.ms24userproductservice.dto.userDto.UserResponseDTO;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PurchaseReportResponseDTO {
    long purchaseId;
    UserResponseDTO user;
    ProductResponseDTO product;
    int discount;
    int quantity;
    BigDecimal totalPrice;
    boolean isRefund;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    LocalDateTime purchaseTime;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    LocalDateTime expiredTime;
}
