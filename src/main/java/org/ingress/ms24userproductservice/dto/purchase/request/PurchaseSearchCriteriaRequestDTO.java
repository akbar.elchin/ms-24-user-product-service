package org.ingress.ms24userproductservice.dto.purchase.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.ingress.ms24userproductservice.repo.repoSpecification.ComparisonOperator;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class PurchaseSearchCriteriaRequestDTO {
    long purchaseId;
    int discountStart;
    int discountEnd;
    ComparisonOperator discountComparison;
    int quantityStart;
    int quantityEnd;
    ComparisonOperator quantityComparison;
    BigDecimal totalPriceStart;
    BigDecimal totalPriceEnd;
    ComparisonOperator totalPriceComparison;
    boolean isRefund;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    LocalDateTime purchaseTimeStart;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    LocalDateTime purchaseTimeEnd;
    ComparisonOperator purchaseTimeComparison;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    LocalDateTime expiredTimeStart;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    LocalDateTime expiredTimeEnd;
    ComparisonOperator expiredTimeComparison;
}
