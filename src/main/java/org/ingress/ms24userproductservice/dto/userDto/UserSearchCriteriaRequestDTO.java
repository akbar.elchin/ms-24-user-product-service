package org.ingress.ms24userproductservice.dto.userDto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import org.ingress.ms24userproductservice.repo.repoSpecification.ComparisonOperator;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class UserSearchCriteriaRequestDTO {
    String username;
    String name;
    String surname;
    @JsonFormat(pattern = "dd/MM/yyyy")
    LocalDate birthDateStart;
    @JsonFormat(pattern = "dd/MM/yyyy")
    LocalDate birthDateEnd;
    ComparisonOperator birthDateComparisonOperator;
    BigDecimal balanceStart;
    BigDecimal balanceEnd;
    ComparisonOperator balanceComparisonOperator;
    boolean status;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    LocalDateTime creatAtStart;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    LocalDateTime creatAtEnd;
    ComparisonOperator creationTimeComparisonOperator;
    int page;
    int size;
}
