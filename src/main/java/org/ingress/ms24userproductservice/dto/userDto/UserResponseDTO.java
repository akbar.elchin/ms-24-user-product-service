package org.ingress.ms24userproductservice.dto.userDto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class UserResponseDTO {
    long id;
    String username;
    String name;
    String surname;
    @JsonFormat(pattern = "dd/MM/yyyy")
    LocalDate birthDate;
    BigDecimal balance;
    boolean status;
    LocalDateTime creatAt;
    LocalDateTime updateAt;
}
