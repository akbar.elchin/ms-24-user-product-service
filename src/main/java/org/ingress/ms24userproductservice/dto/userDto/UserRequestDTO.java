package org.ingress.ms24userproductservice.dto.userDto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRequestDTO {
    String username;
    String name;
    String surname;
    @JsonFormat(pattern = "dd/MM/yyyy")
    LocalDate birthDate;
    BigDecimal balance;
}
