package org.ingress.ms24userproductservice.dto.product;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
@Data
@Builder
@FieldDefaults(level = AccessLevel.PACKAGE)
public class ProductRequestDTO {
    String name;
    BigDecimal price;
    int stockCount;
}
