package org.ingress.ms24userproductservice.dto.product;

import lombok.Data;
import org.ingress.ms24userproductservice.repo.repoSpecification.ComparisonOperator;

import java.math.BigDecimal;

@Data
public class ProductSearchCriteriaRequestDTO {
    String name;
    BigDecimal priceStart;
    BigDecimal priceEnd;
    ComparisonOperator priceOperator;
    int stockCountStart;
    int stockCountEnd;
    ComparisonOperator stockCountOperator;
    int page;
    int size;
}
