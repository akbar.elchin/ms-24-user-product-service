package org.ingress.ms24userproductservice.repo;

import org.ingress.ms24userproductservice.entity.User;
import org.ingress.ms24userproductservice.entity.UserBalance;
import org.ingress.ms24userproductservice.repo.repoSpecification.UserJpaSpecification;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User>, UserJpaSpecification {

    Optional<UserBalance> findBalanceByUsername(String username);

    Optional<User> getUserByUsername(String username);

    List<User> getAllByStatus(PageRequest request, boolean status);

    @Modifying
    @Query("UPDATE User u SET u.balance = u.balance + :amount WHERE u.id = :userId")
    void increaseBalanceById(long userId, BigDecimal amount);
}
