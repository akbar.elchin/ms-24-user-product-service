package org.ingress.ms24userproductservice.repo;

import org.ingress.ms24userproductservice.entity.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PurchaseRepository extends JpaRepository<Purchase, Long> {
}
