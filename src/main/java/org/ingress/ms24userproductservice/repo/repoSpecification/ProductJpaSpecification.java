package org.ingress.ms24userproductservice.repo.repoSpecification;

import jakarta.persistence.criteria.Predicate;
import org.ingress.ms24userproductservice.dto.product.ProductSearchCriteriaRequestDTO;
import org.ingress.ms24userproductservice.entity.Product;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

import static org.ingress.ms24userproductservice.repo.repoSpecification.CommonSpecification.*;

public interface ProductJpaSpecification {

    default Specification<Product> createSpecification(ProductSearchCriteriaRequestDTO requestDTO) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            addLikePredicateIfPresent(requestDTO.getName(), root.get("name"), criteriaBuilder, predicates);
            addIntegerPredicateIfPresent(requestDTO.getStockCountStart(), requestDTO.getStockCountEnd(),
                    requestDTO.getStockCountOperator(), root.get("stockCount"), criteriaBuilder, predicates);
            addBigDecimalPredicateIfPresent(requestDTO.getPriceStart(), requestDTO.getPriceEnd(),
                    requestDTO.getPriceOperator(), root.get("balance"), criteriaBuilder, predicates);
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
