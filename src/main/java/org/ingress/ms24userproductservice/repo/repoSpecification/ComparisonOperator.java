package org.ingress.ms24userproductservice.repo.repoSpecification;

public enum ComparisonOperator {
    EQUAL,
    GREATER_THAN,
    LESS_THAN,
    BETWEEN
}
