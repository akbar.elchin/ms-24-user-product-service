package org.ingress.ms24userproductservice.repo.repoSpecification;

import jakarta.persistence.criteria.Predicate;
import org.ingress.ms24userproductservice.dto.userDto.UserSearchCriteriaRequestDTO;
import org.ingress.ms24userproductservice.entity.User;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

import static org.ingress.ms24userproductservice.repo.repoSpecification.CommonSpecification.*;

public interface UserJpaSpecification {

    default Specification<User> createSpecification(UserSearchCriteriaRequestDTO requestDTO) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            addLikePredicateIfPresent(requestDTO.getUsername(), root.get("username"), criteriaBuilder, predicates);
            addLikePredicateIfPresent(requestDTO.getName(), root.get("name"), criteriaBuilder, predicates);
            addLikePredicateIfPresent(requestDTO.getSurname(), root.get("surname"), criteriaBuilder, predicates);
            addEqualPredicateIfPresent(requestDTO.isStatus(), root.get("status"), criteriaBuilder, predicates);
            addDatePredicateIfPresent(requestDTO.getBirthDateStart(), requestDTO.getBirthDateEnd(),
                    requestDTO.getBirthDateComparisonOperator(), root.get("birthDate"), criteriaBuilder, predicates);
            addDateTimePredicateIfPresent(requestDTO.getCreatAtStart(), requestDTO.getCreatAtEnd(),
                    requestDTO.getCreationTimeComparisonOperator(), root.get("creatAt"), criteriaBuilder, predicates);
            addBigDecimalPredicateIfPresent(requestDTO.getBalanceStart(), requestDTO.getBalanceEnd(),
                    requestDTO.getBalanceComparisonOperator(), root.get("balance"), criteriaBuilder, predicates);
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
