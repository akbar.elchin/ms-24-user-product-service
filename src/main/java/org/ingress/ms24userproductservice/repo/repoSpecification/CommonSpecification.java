package org.ingress.ms24userproductservice.repo.repoSpecification;

import jakarta.persistence.criteria.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface CommonSpecification {

    static void addLikePredicateIfPresent(String value,
                                          Path<String> attribute,
                                          CriteriaBuilder criteriaBuilder,
                                          List<Predicate> predicates) {
        Optional.ofNullable(value)
                .filter(val -> !val.isEmpty())
                .map(val -> criteriaBuilder.like(attribute, "%" + val + "%"))
                .ifPresent(predicates::add);
    }

    static <T> void addEqualPredicateIfPresent(T value, Path<T> attribute,
                                               CriteriaBuilder criteriaBuilder,
                                               List<Predicate> predicates) {
        Optional.ofNullable(value)
                .map(val -> criteriaBuilder.equal(attribute, val))
                .ifPresent(predicates::add);
    }

    static void addIsFalsePredicateIfPresent(Path<Boolean> attribute,
                                             CriteriaBuilder criteriaBuilder,
                                             List<Predicate> predicates) {
        predicates.add(criteriaBuilder.isFalse(attribute));
    }

    static void addIsTruePredicateIfPresent(Path<Boolean> attribute,
                                            CriteriaBuilder criteriaBuilder,
                                            List<Predicate> predicates) {
        predicates.add(criteriaBuilder.isTrue(attribute));
    }

    static void addDatePredicateIfPresent(LocalDate start, LocalDate end,
                                          ComparisonOperator operator,
                                          Path<LocalDate> attribute,
                                          CriteriaBuilder criteriaBuilder,
                                          List<Predicate> predicates) {
        if (start != null) {
            switch (operator) {
                case EQUAL -> predicates.add(criteriaBuilder.equal(attribute, start));
                case GREATER_THAN -> predicates.add(criteriaBuilder.greaterThan(attribute, start));
                case LESS_THAN -> predicates.add(criteriaBuilder.lessThan(attribute, start));
                case BETWEEN -> {
                    if (end != null) {
                        predicates.add(criteriaBuilder.between(attribute, start, end));
                    }
                }
            }
        }
    }

    static void addDateTimePredicateIfPresent(LocalDateTime start, LocalDateTime end,
                                              ComparisonOperator operator,
                                              Path<LocalDateTime> attribute,
                                              CriteriaBuilder criteriaBuilder,
                                              List<Predicate> predicates) {
        if (start != null) {
            switch (operator) {
                case EQUAL -> predicates.add(criteriaBuilder.equal(attribute, start));
                case GREATER_THAN -> predicates.add(criteriaBuilder.greaterThan(attribute, start));
                case LESS_THAN -> predicates.add(criteriaBuilder.lessThan(attribute, start));
                case BETWEEN -> {
                    if (end != null) {
                        predicates.add(criteriaBuilder.between(attribute, start, end));
                    }
                }
            }
        }
    }

    static void addBigDecimalPredicateIfPresent(BigDecimal value1,
                                                BigDecimal value2,
                                                ComparisonOperator operator,
                                                Path<BigDecimal> attribute,
                                                CriteriaBuilder criteriaBuilder,
                                                List<Predicate> predicates) {
        if (value1 != null) {
            switch (operator) {
                case EQUAL -> predicates.add(criteriaBuilder.equal(attribute, value1));
                case GREATER_THAN -> predicates.add(criteriaBuilder.greaterThan(attribute, value1));
                case LESS_THAN -> predicates.add(criteriaBuilder.lessThan(attribute, value1));
                case BETWEEN -> {
                    if (value2 != null) {
                        predicates.add(criteriaBuilder.between(attribute, value1, value2));
                    }
                }
            }
        }
    }

    static void addIntegerPredicateIfPresent(Integer value1,
                                             Integer value2,
                                             ComparisonOperator operator,
                                             Path<Integer> attribute,
                                             CriteriaBuilder criteriaBuilder,
                                             List<Predicate> predicates) {
        if (value1 != null) {
            switch (operator) {
                case EQUAL -> predicates.add(criteriaBuilder.equal(attribute, value1));
                case GREATER_THAN -> predicates.add(criteriaBuilder.greaterThan(attribute, value1));
                case LESS_THAN -> predicates.add(criteriaBuilder.lessThan(attribute, value1));
                case BETWEEN -> {
                    if (value2 != null) {
                        predicates.add(criteriaBuilder.between(attribute, value1, value2));
                    }
                }
            }
        }
    }

    static <N extends Number> void addMinPredicateIfPresent(Path<N> attribute,
                                                            CriteriaBuilder criteriaBuilder,
                                                            List<Predicate> predicates) {
        predicates.add(criteriaBuilder.ge(attribute, getMinSelection(attribute, criteriaBuilder)));
    }

    static <N extends Number> void addMaxPredicateIfPresent(Path<N> attribute,
                                                            CriteriaBuilder criteriaBuilder,
                                                            List<Predicate> predicates) {
        predicates.add(criteriaBuilder.le(attribute, getMaxSelection(attribute, criteriaBuilder)));
    }

    static <N extends Number> void addSumPredicateIfPresent(Path<N> attribute,
                                                            CriteriaBuilder criteriaBuilder,
                                                            List<Predicate> predicates) {
        predicates.add(criteriaBuilder.isNotNull(attribute));
    }

    static <N extends Number> void addAvgPredicateIfPresent(Path<N> attribute,
                                                            CriteriaBuilder criteriaBuilder,
                                                            List<Predicate> predicates) {
        predicates.add(criteriaBuilder.isNotNull(attribute));
    }

    private static <N extends Number> Expression<? extends Number> getMinSelection(Path<N> attribute, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.min(attribute);
    }

    private static <N extends Number> Expression<? extends Number> getMaxSelection(Path<N> attribute, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.max(attribute);
    }
}
