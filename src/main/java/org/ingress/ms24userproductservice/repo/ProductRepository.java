package org.ingress.ms24userproductservice.repo;

import org.ingress.ms24userproductservice.entity.Product;
import org.ingress.ms24userproductservice.repo.repoSpecification.ProductJpaSpecification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ProductRepository extends JpaRepository<Product, Long>, JpaSpecificationExecutor<Product>, ProductJpaSpecification {

    @Modifying
    @Query("UPDATE Product p SET p.stockCount = p.stockCount + :stockCount WHERE p.id = :productId")
    void increaseStockCountById(long productId, int stockCount);
}
