package org.ingress.ms24userproductservice.entity;

import java.math.BigDecimal;

public interface UserBalance {
    BigDecimal getBalance();
}
