package org.ingress.ms24userproductservice.mapper;

import java.util.List;

public interface BaseMapper<REQ, RES, E> {

    E mapToEntity(REQ req);

    E mapToEntity(REQ req, E e);

    RES mapToResponse(E e);

    List<RES> mapToResponseList(List<E> entities);
}
