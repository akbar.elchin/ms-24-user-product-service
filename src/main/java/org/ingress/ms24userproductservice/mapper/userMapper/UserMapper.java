package org.ingress.ms24userproductservice.mapper.userMapper;

import org.ingress.ms24userproductservice.dto.userDto.UserRequestDTO;
import org.ingress.ms24userproductservice.dto.userDto.UserResponseDTO;
import org.ingress.ms24userproductservice.entity.User;
import org.ingress.ms24userproductservice.mapper.BaseMapper;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserMapper implements BaseMapper<UserRequestDTO, UserResponseDTO, User> {

    @Override
    public User mapToEntity(UserRequestDTO requestDTO) {
        User user = new User();
        user.setUsername(requestDTO.getUsername());
        user.setName(requestDTO.getName());
        user.setSurname(requestDTO.getSurname());
        user.setBirthDate(requestDTO.getBirthDate());
        user.setBalance(requestDTO.getBalance());
        user.setStatus(true);
        user.setCreatAt(LocalDateTime.now());
        return user;
    }

    @Override
    public User mapToEntity(UserRequestDTO requestDTO, User user) {

        if (!ObjectUtils.isEmpty(requestDTO.getUsername())) {
            user.setUsername(requestDTO.getUsername());
        }
        if (!ObjectUtils.isEmpty(requestDTO.getName())) {
            user.setName(requestDTO.getName());
        }
        if (!ObjectUtils.isEmpty(requestDTO.getSurname())) {
            user.setSurname(requestDTO.getSurname());
        }
        if (requestDTO.getBirthDate() != null) {
            user.setBirthDate(requestDTO.getBirthDate());
        }
        if (requestDTO.getBalance() != null) {
            user.setBalance(requestDTO.getBalance());
        }
        user.setUpdateAt(LocalDateTime.now());
        return user;
    }

    @Override
    public UserResponseDTO mapToResponse(User user) {
        return UserResponseDTO.builder()
                .id(user.getId())
                .username(user.getUsername())
                .name(user.getName())
                .surname(user.getSurname())
                .balance(user.getBalance())
                .birthDate(user.getBirthDate())
                .status(user.isStatus())
                .creatAt(user.getCreatAt())
                .updateAt(user.getUpdateAt())
                .build();
    }

    @Override
    public List<UserResponseDTO> mapToResponseList(List<User> entities) {
        return entities.stream().map(this::mapToResponse).collect(Collectors.toList());
    }
}
