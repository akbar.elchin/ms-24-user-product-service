package org.ingress.ms24userproductservice.mapper.purchaseMapper;

import lombok.RequiredArgsConstructor;
import org.ingress.ms24userproductservice.dto.product.ProductResponseDTO;
import org.ingress.ms24userproductservice.dto.purchase.request.PurchaseBuyRequestDTO;
import org.ingress.ms24userproductservice.dto.purchase.response.PurchaseBuyResponseDTO;
import org.ingress.ms24userproductservice.dto.purchase.response.PurchaseRefundResponseDTO;
import org.ingress.ms24userproductservice.dto.purchase.response.PurchaseReportResponseDTO;
import org.ingress.ms24userproductservice.dto.userDto.UserResponseDTO;
import org.ingress.ms24userproductservice.entity.Product;
import org.ingress.ms24userproductservice.entity.Purchase;
import org.ingress.ms24userproductservice.entity.User;
import org.ingress.ms24userproductservice.mapper.productMapper.ProductMapper;
import org.ingress.ms24userproductservice.mapper.userMapper.UserMapper;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class PurchaseMapper {

    private final UserMapper userMapper;
    private final ProductMapper productMapper;

    public PurchaseBuyResponseDTO mapToResponse(Purchase purchase) {
        return PurchaseBuyResponseDTO.builder()
                .purchaseId(purchase.getId())
                .userId(purchase.getUser().getId())
                .productId(purchase.getProduct().getId())
                .productName(purchase.getProduct().getName())
                .productPrice(purchase.getProduct().getPrice())
                .discount(purchase.getDiscount())
                .quantity(purchase.getQuantity())
                .totalPrice(purchase.getTotalPrice())
                .purchaseTime(purchase.getPurchaseTime())
                .expiredTime(purchase.getExpireRefundDate())
                .build();
    }

    public PurchaseRefundResponseDTO mapToRefundResponse(Purchase purchase) {
        return PurchaseRefundResponseDTO.builder()
                .purchaseId(purchase.getId())
                .userId(purchase.getUser().getId())
                .productId(purchase.getProduct().getId())
                .productName(purchase.getProduct().getName())
                .productPrice(purchase.getProduct().getPrice())
                .discount(purchase.getDiscount())
                .quantity(purchase.getQuantity())
                .refundAmount(purchase.getTotalPrice())
                .refundTime(purchase.getRefundTime())
                .expiredTime(purchase.getExpireRefundDate())
                .isRefund(purchase.isRefund())
                .build();
    }

    public List<PurchaseBuyResponseDTO> mapToResponseList(List<Purchase> entities) {
        return entities.stream().map(this::mapToResponse).collect(Collectors.toList());
    }

    public List<PurchaseRefundResponseDTO> mapToRefundResponseList(List<Purchase> entities) {
        return entities.stream().map(this::mapToRefundResponse).collect(Collectors.toList());
    }

    public Purchase mapToEntity(PurchaseBuyRequestDTO requestDTO, User user, Product product,
                                BigDecimal totalPrice) {
        Purchase purchase = new Purchase();
        purchase.setUser(user);
        purchase.setProduct(product);
        purchase.setQuantity(requestDTO.getQuantity());
        purchase.setDiscount(requestDTO.getDiscount());
        purchase.setTotalPrice(totalPrice);
        purchase.setPurchaseTime(LocalDateTime.now());
        purchase.setExpireRefundDate(LocalDateTime.now().plusDays(14));
        return purchase;
    }

    public PurchaseReportResponseDTO mapToReportResponse(Purchase purchase) {
        UserResponseDTO user = userMapper.mapToResponse(purchase.getUser());
        ProductResponseDTO product = productMapper.mapToResponse(purchase.getProduct());
        return PurchaseReportResponseDTO.builder()
                .purchaseId(purchase.getId())
                .user(user)
                .product(product)
                .discount(purchase.getDiscount())
                .quantity(purchase.getQuantity())
                .totalPrice(purchase.getTotalPrice())
                .purchaseTime(purchase.getPurchaseTime())
                .expiredTime(purchase.getExpireRefundDate())
                .isRefund(purchase.isRefund())
                .build();
    }

    public List<PurchaseReportResponseDTO> mapToReportResponseList(List<Purchase> purchases) {
        return purchases.stream().map(this::mapToReportResponse).collect(Collectors.toList());
    }

}
