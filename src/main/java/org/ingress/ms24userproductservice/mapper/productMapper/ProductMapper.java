package org.ingress.ms24userproductservice.mapper.productMapper;

import org.ingress.ms24userproductservice.dto.product.ProductRequestDTO;
import org.ingress.ms24userproductservice.dto.product.ProductResponseDTO;
import org.ingress.ms24userproductservice.entity.Product;
import org.ingress.ms24userproductservice.mapper.BaseMapper;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductMapper implements BaseMapper<ProductRequestDTO, ProductResponseDTO, Product> {
    @Override
    public Product mapToEntity(ProductRequestDTO productRequestDTO) {
        Product product = new Product();
        product.setName(productRequestDTO.getName());
        product.setPrice(product.getPrice());
        product.setStockCount(productRequestDTO.getStockCount());
        return product;
    }

    @Override
    public Product mapToEntity(ProductRequestDTO productRequestDTO, Product product) {

        if (!ObjectUtils.isEmpty(productRequestDTO.getName())) {
            product.setName(productRequestDTO.getName());
        }
        if (!ObjectUtils.isEmpty(productRequestDTO.getPrice())) {
            product.setPrice(productRequestDTO.getPrice());
        }
        if (!ObjectUtils.isEmpty(productRequestDTO.getStockCount())) {
            product.setStockCount(productRequestDTO.getStockCount());
        }
        return product;
    }

    @Override
    public ProductResponseDTO mapToResponse(Product product) {
        return ProductResponseDTO.builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .stockCount(product.getStockCount())
                .build();
    }

    @Override
    public List<ProductResponseDTO> mapToResponseList(List<Product> entities) {
        return entities.stream().map(this::mapToResponse).collect(Collectors.toList());
    }
}
